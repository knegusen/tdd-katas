class StringCalculator {

    add(numbers) {

        if (numbers === '') {
            return '0';
        }

        var delimiter = this._getDelimiter(numbers);
        var numberArray = this._numbersToArray(delimiter, numbers);
        var sum = this._sumFromArray(numberArray);

        return String(sum);
    }

    _numbersToArray(delimiter, numbers) {

        if (this._hasUserSpecifiedDelimiter(numbers)) {
            numbers = numbers.split('\n')[1];
        }

        return numbers.split(delimiter);
    }

    _hasUserSpecifiedDelimiter(numbers) {
        return numbers.substring(0, 2) === '//';
    }

    _getDelimiter(numbers) {

        var delimiter = /[\n,]/g;

        if (this._hasUserSpecifiedDelimiter(numbers)) {

            numbers = this._removeStartOfDelimiterDefinition(numbers);
            delimiter = numbers.split('\n')[0];

            if (this._hasSeveralDelimiters(numbers)) {
                delimiter = this._createDelimiterRegExp(delimiter);
            }
        }

        return delimiter;
    }

    _removeStartOfDelimiterDefinition(numbers) {
        return numbers.substring(2, numbers.length);
    }

    _hasSeveralDelimiters(delimiter) {
        return delimiter.substring(0, 1) === '[';
    }

    _removeMiddleBrackets(delimiter) {
        return delimiter.replace(/\]\[/g, '');
    }

    _createDelimiterRegExp(delimiter) {
        delimiter = this._removeMiddleBrackets(delimiter);
        delimiter = new RegExp(delimiter, 'g');
        return delimiter;
    }

    _sumFromArray(numberArray) {
        var sum = 0;
        for (var i = 0; i < numberArray.length; ++i) {
            var num = parseInt(numberArray[i]);
            if (num < 0) {
                throw 'Negative digits are not allowed!';
            }
            if (num < 1000) {
                sum += num;
            }
        }
        return sum;
    }
}

export default StringCalculator;