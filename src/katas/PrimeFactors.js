export default class PrimeFactors {
    static getPrimes(number) {
        var list = [];
        var candidate = 2;
        while (number > 1) {
            while (number % candidate == 0) {
                list.push(candidate);
                number /= candidate;
            }
            candidate++;
        }
        return list;
    }
}