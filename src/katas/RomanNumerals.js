class RomanNumerals {

    static toRoman(number) {

        var roman = {
            value: ""
        };

        var remaining = number;

        var values = [1000, 900, 500, 400, 100, 90, 50, 40, 10, 9, 5, 4, 1];
        var symbols = ["M", "CM", "D", "CD", "C", "XC", "L", "XL", "X", "IX", "V", "IV", "I"];

        for (var i = 0; i < values.length; i++) {
            remaining = this.appendNumerals(remaining, roman, values[i], symbols[i]);
        }

        return roman.value;
    }

    static appendNumerals(remaining, roman, n, s) {
        while (remaining >= n) {
            roman.value += s;
            remaining -= n;
        }
        return remaining;
    }
}

export default RomanNumerals;