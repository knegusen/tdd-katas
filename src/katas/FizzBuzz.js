export default class FizzBuzz {

    static run(n) {
        let fizzBuzzArray = this._initializeArray(n);
        fizzBuzzArray = fizzBuzzArray.map((elem) => {
            let value = '';

            if (elem % 3 === 0) {
                value += 'fizz';
            }

            if (elem % 5 === 0) {
                value += 'buzz';
            }

            return (value || elem);

        });
        return fizzBuzzArray;
    }

    static _initializeArray(n) {
        let array = [];
        for (let i = 1; i <= n; ++i) {
            array.push(i);
        }
        return array;
    }
}
