import React from 'react'
import ExampleComponent from './ExampleComponent'

React.render(<ExampleComponent>This is the example component</ExampleComponent>, document.getElementById('content'));
