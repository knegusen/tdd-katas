import BowlingGame from "../../src/katas/BowlingGame";

describe("BowlingGame", () => {

    var bowlingGame;

    beforeEach(()=> {
        bowlingGame = new BowlingGame();
    });

    function rollMany(n, pins) {
        for (var i = 0; i < n; i++) {
            bowlingGame.roll(pins);
        }
    }

    function rollSpare() {
        bowlingGame.roll(5);
        bowlingGame.roll(5);
    }

    function rollStrike() {
        bowlingGame.roll(10);
    }

    it("can roll a gutter game", () => {
        rollMany(20, 0);
        expect(bowlingGame.getScore()).toBe(0);
    });

    it("can roll all ones", () => {
        rollMany(20, 1);
        expect(bowlingGame.getScore()).toBe(20);
    });

    it("can roll one spare", () => {
        rollSpare();
        bowlingGame.roll(3);
        rollMany(17, 0);
        expect(bowlingGame.getScore()).toBe(16);
    });

    it("can roll one strike", () => {
        rollStrike();
        bowlingGame.roll(3);
        bowlingGame.roll(4);
        rollMany(16, 0);
        expect(bowlingGame.getScore()).toBe(24);
    });

    it("can roll only strikes", ()=> {
        rollMany(12, 10);
        expect(bowlingGame.getScore()).toBe(300);
    });

    it("can roll spare in last square", () => {
        rollMany(18, 0);
        bowlingGame.roll(5);
        bowlingGame.roll(5);
        bowlingGame.roll(5);
        expect(bowlingGame.getScore()).toBe(15);
    });

    it("can roll three strike in last square", () => {
        rollMany(18, 0);
        bowlingGame.roll(10);
        bowlingGame.roll(10);
        bowlingGame.roll(10);
        expect(bowlingGame.getScore()).toBe(30);
    });
});
