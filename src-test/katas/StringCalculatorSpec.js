import StringCalculator from '../../src/katas/StringCalculator';

describe('StringCalculator', () => {

    describe('add', () => {

        var stringCalculator;

        beforeEach(() => {
            stringCalculator = new StringCalculator();
        });

        it('returns 0 for an empty string', () => {
            expect(stringCalculator.add('')).toBe('0');
        });

        it('returns the number if only one is present', () => {
            expect(stringCalculator.add('1')).toBe('1');
        });

        it('throws an error on negative digits', () => {
            expect(() => {
                stringCalculator.add('-1')
            }).toThrow();
        });

        describe('with two or more numbers', () => {

            it('ignores digits larger than 1000', () => {
                expect(stringCalculator.add('2,1001')).toBe('2');
            });

            describe('without specified delimiters', () => {

                it('returns the correct sum when numbers are only comma separated', () => {
                    expect(stringCalculator.add('1,2,3')).toBe('6');
                });

                it('returns the correct sum when numbers are separated only with a newline', () => {
                    expect(stringCalculator.add('1\n2')).toBe('3');
                });

                it('returns the correct sum when numbers are separated with both newline and comma', () => {
                    expect(stringCalculator.add('1\n2,3')).toBe('6');
                });

            });

            describe('with specified delimiters', () => {

                it('returns the correct sum with one delimiter of length one', () => {
                    expect(stringCalculator.add("//;\n1;2")).toBe('3');
                });

                it('returns the correct sum with one delimiter of length greater than one', () => {
                    expect(stringCalculator.add('//;;;\n1;;;2;;;3')).toBe('6');
                });

                it('returns the correct sum with two different delimiters of length one', () => {
                    expect(stringCalculator.add('//[;][*]\n1;2*3')).toBe('6');
                });

                it('returns the correct sum with two different delimiters of length greater than one', () => {
                    expect(stringCalculator.add('//[;;;][**]\n1;;;2**3')).toBe('6');
                });

            });

        });

    });

});