import PrimeFactors from "../../src/katas/PrimeFactors";

describe("primefactors", () => {
    function expectPrimes(number, primeList) {
        expect(PrimeFactors.getPrimes(number)).toEqual(primeList);
    }

    it("returns correct list of primes", () => {
        expectPrimes(1, []);
        expectPrimes(2, [2]);
        expectPrimes(3, [3]);
        expectPrimes(4, [2, 2]);
        expectPrimes(5, [5]);
        expectPrimes(6, [2, 3]);
        expectPrimes(7, [7]);
        expectPrimes(8, [2, 2, 2]);
        expectPrimes(9, [3, 3]);
        expectPrimes(2343, [3, 11, 71]);
    });
});